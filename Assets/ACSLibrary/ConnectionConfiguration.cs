﻿using System;

namespace ACS
{
	public class ConnectionConfiguration
	{
		public readonly static int Port = 35288;
		public readonly static short ClientMessageType = 99;
		public readonly static short ServerMessageType = 98;
	}
}

