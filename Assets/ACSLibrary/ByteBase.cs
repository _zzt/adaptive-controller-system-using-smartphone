﻿using System;

using UnityEngine.Networking;

namespace ACS {
	public class ByteBase: MessageBase {
		public byte[] data;

		public ByteBase(byte[] data) {
			this.data = data;
		}

		public override void Serialize (NetworkWriter writer)
		{
			writer.WriteBytesFull (data);
		}

		public override void Deserialize (NetworkReader reader)
		{
			data = reader.ReadBytesAndSize ();
		}
	}
}

