﻿using System;
using System.Xml;
using System.Xml.Serialization;

namespace ACS.UI
{
	[Serializable]
	public class OneAxisWheelUIData: UIDataAdditional
	{
		[XmlElement] public bool maxExist;
		[XmlElement] public float max_deg;

		[XmlElement] public bool minExist;
		[XmlElement] public float min_deg;

		[XmlElement] public bool autoReset;
		[XmlElement] public float autoResetValue;

		[XmlElement] public string img_handle;
		[XmlElement] public float handle_w;
		[XmlElement] public float handle_h;
	}
}

