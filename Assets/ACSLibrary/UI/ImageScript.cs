﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ACS.UI {
	public class ImageScript : UITemplateBase
	{
		public RawImage img;

		[SerializeField] public ImageUIData uiData = new ImageUIData ();

		public ImageScript() : base()
		{
			type = UIType.Image;
		}

		public override string GetMessage()
		{
			return "";
		}

		public override void ApplyUIData ()
		{
			UIXMLParser.Instance.DecodeRawImage (img, uiData.img);
		}

		public override void UpdateUIData ()
		{
			uiData.img = UIXMLParser.Instance.EncodeRawImage (img);
		}
	}
}
