﻿using System;
using System.Xml;
using System.Xml.Serialization;

namespace ACS.UI
{
	[Serializable]
	public class TwoAxisUIData: UIDataAdditional
	{
		[XmlElement] public string img_handle;
		[XmlElement] public string img_background;
        [XmlElement] public float width;
        [XmlElement] public float height;
	}
}

