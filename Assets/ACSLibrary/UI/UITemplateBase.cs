﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_ANDROID
using ACS.App;
#endif

namespace ACS.UI
{
    public abstract class UITemplateBase : MonoBehaviour
    {
        public bool IsActive = false;
        public bool CanControl = false;

        public int uid;
		public string uiTag;

        public bool IsEnable { get { return IsActive && CanControl; } }

        public UIType type;

		public void SyncState () {
			#if UNITY_ANDROID && !UNITY_EDITOR
			Sender.Instance.SendSignal (GetMessage ());
			#endif
		}

        public abstract string GetMessage();

		public virtual void ApplyUIData() {}
		public virtual void UpdateUIData() {}
    }
}