﻿using System.Collections;
using System.Collections.Generic;

namespace ACS.UI
{
    public class ClickButtonState : UIState
    {
      	bool _pressed = false;
		public bool prev;
		public bool curr {
			get { return GetBool(); }
		}

		public void ChangeBool(bool value) {
			_pressed = value;
		}

        public override void ChangeUIState(string s)
        {
			_pressed = bool.Parse (s);
        }

        public override float GetFloat()
        {
            return _pressed? 1 : 0;
        }

        public override bool GetBool() {
			return _pressed;
		}

		public override string Encode ()
		{
			return _pressed.ToString ();
		}
    }
}
