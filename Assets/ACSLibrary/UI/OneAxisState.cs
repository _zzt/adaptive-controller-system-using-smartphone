﻿using System.Collections;
using System.Collections.Generic;

namespace ACS.UI
{
	public class OneAxisState : UIState
	{
		float _value;

		public void ChangeFloat(float value) {
			_value = value;
		}

		public override void ChangeUIState(string s)
		{
			_value = float.Parse (s);
		}

        public override bool GetBool()
        {
            return _value > 0;
        }

        public override float GetFloat() {
			return _value;
		}

		public override string Encode ()
		{
			return _value.ToString ();
		}
	}
}
