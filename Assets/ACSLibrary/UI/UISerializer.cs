﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Text;

namespace ACS.UI {
    [Serializable]
    public enum UIType
    {
		Image,
        ClickButton,
        OneAxisSlider,
        OneAxisJoystic,
		OneAxisWheel,
        TwoAxis,
    }
    
    [Serializable]
    public class UISerializedDataList
    {
        public int length { get { return list.Count; } }

        [XmlElement]
        public List<UISerializedData> list = new List<UISerializedData>();
    }

    [Serializable]
    public class UISerializedData
    {
        [XmlElement]
        public Vector3 pos, scale;
        [XmlElement]
        public Vector2 am, aM, pivot, anch, wh;
        [XmlElement]
        public Quaternion rot;
        [XmlElement]
        public UIType type;

        [XmlElement]
        public Vector2 rpos, rcenter, rmin, rmax, rsize;
        public float rx, ry, rwidth, rheight, rxmin, rymin, rxmax, rymax;
        
        [XmlElement]
        public int uid;

		[XmlElement]
		public string tag;

		[XmlElement] public ImageUIData uiData_img = null;
		[XmlElement] public ClickButtonUIData uiData_0D = null;
		[XmlElement] public OneAxisUIData uiData_1D = null;
		[XmlElement] public TwoAxisUIData uiData_2D = null;
		[XmlElement] public OneAxisWheelUIData uiData_1WD = null;


        public UISerializedData()
        {
        }

        public UISerializedData(UITemplateBase t)
        {
            Initialize(t);
        }

        public UISerializedData(UITemplateBase t, Rect r)
        {
            Initialize(t);
            pos = pos + new Vector3(r.width/2, r.height/2);
        }

        void Initialize(UITemplateBase t)
        {
            type = t.type;
            var tRect = t.GetComponent<RectTransform>();

            pos = tRect.localPosition; scale = tRect.localScale;
            am = tRect.anchorMin; aM = tRect.anchorMax;
            rot = tRect.rotation; pivot = tRect.pivot;
            anch = tRect.anchoredPosition;
            wh = tRect.sizeDelta;

            rx = tRect.rect.x;
            ry = tRect.rect.y;
            rpos = tRect.rect.position;
            rcenter = tRect.rect.center;
            rmin = tRect.rect.min;
            rmax = tRect.rect.max;
            rwidth = tRect.rect.width;
            rheight = tRect.rect.height;
            rsize = tRect.rect.size;
            rxmax = tRect.rect.xMax;
            rxmin = tRect.rect.xMin;
            rymax = tRect.rect.yMax;
            rymin = tRect.rect.yMin;

			tag = t.uiTag;

			if (t is ImageScript) {
				uiData_img = ((ImageScript)t).uiData;
			}
			if (t is ClickButtonScript) {
				uiData_0D = ((ClickButtonScript)t).uiData;
			}
			if (t is OneAxisScript) {
				uiData_1D = ((OneAxisScript)t).uiData;
			}
            
            if (t is TwoAxisScript) {
				uiData_2D = ((TwoAxisScript)t).uiData;
			}

            if (t is OneAxisJoysticScript)
            {
                uiData_2D = ((OneAxisJoysticScript)t).uiData;
			}

			if (t is OneAxisWheelScript)
			{
				uiData_1WD = ((OneAxisWheelScript)t).uiData;
			}
        }

		public void ApplyTo(UITemplateBase t) {
			var rect = t.gameObject.GetComponent<RectTransform>();

			CopyRectTransform(rect);
			t.uid = uid;
			t.uiTag = tag;

			if (t is ImageScript) {
				ImageScript t0 = (ImageScript)t;
				if (uiData_img != null) {
					t0.uiData = uiData_img;
				}
				t0.ApplyUIData ();
			}
			if (t is ClickButtonScript) {
				ClickButtonScript t0 = (ClickButtonScript)t;
				if (uiData_0D != null) {
					t0.uiData = uiData_0D;
				}
				t0.ApplyUIData ();
			}
			if (t is OneAxisScript) {
				OneAxisScript t1 = (OneAxisScript)t;
				if (uiData_1D != null) {
					t1.uiData = uiData_1D;
				}
				t1.ApplyUIData ();
			}
			if (t is TwoAxisScript) {
				TwoAxisScript t2 = (TwoAxisScript)t;
				if (uiData_2D != null) {
					t2.uiData = uiData_2D;
				}
				t2.ApplyUIData ();
			}

            if (t is OneAxisJoysticScript)
            {
                OneAxisJoysticScript t2 = (OneAxisJoysticScript)t;
                if (uiData_2D != null)
                {
                    t2.uiData = uiData_2D;
                }
                t2.ApplyUIData();
            }


			if (t is OneAxisWheelScript) {
				OneAxisWheelScript t2 = (OneAxisWheelScript)t;
				if (uiData_1WD != null) {
					t2.uiData = uiData_1WD;
				}
				t2.ApplyUIData ();
			}
        }

        public RectTransform CopyRectTransform(RectTransform _rect)
        {
            _rect.pivot = pivot;

            _rect.anchorMin = am;
            _rect.anchorMax = aM;          
            
            _rect.rotation = rot;
            
            _rect.sizeDelta = wh;

            _rect.anchoredPosition = anch;

            //_rect.localPosition = pos;

            _rect.localScale = scale;

            //_rect.rect.Set(rx, ry, rwidth, rheight);


            return _rect;
        }
    }

    public static class UISerializer {
        public static Dictionary<UIType, string> dic = new Dictionary<UIType, string>()
            {{UIType.ClickButton, "ClickButton" },
            {UIType.OneAxisSlider, "Slider" } ,
			{UIType.OneAxisJoystic, "OneAxisJoystic" },
			{UIType.OneAxisWheel, "OneAxisWheel" },
			{UIType.TwoAxis, "TwoAxis" },
			{UIType.Image, "Image" },
		};
		
		public static void SaveUIDataToXml(UISerializedDataList list, string path)
		{
			XmlSerializer xml = new XmlSerializer(typeof(UISerializedDataList));

			using (FileStream stream = new FileStream(path, FileMode.Create))
			{
				StreamWriter wr = new StreamWriter(stream, Encoding.UTF8);
				xml.Serialize(wr, list);
			}
		}

        public static void SaveUIDataToXml(UISerializedDataList list)
        {
			SaveUIDataToXml (list, "Assets/Resources/UI/" + "testUI.xml");
        }

        public static void SaveUIDataToXml(UISerializedData data)
        {
            XmlSerializer xml = new XmlSerializer(typeof(UISerializedData));
            
            using( FileStream stream = new FileStream("Assets/Resources/UI/" + "testUI.xml", FileMode.Create))
            {
                StreamWriter wr = new StreamWriter(stream, Encoding.UTF8);
                xml.Serialize(wr, data);
            }
        }

        public static UISerializedDataList LoadUIDataFromFile(string name)
        {
            XmlSerializer xml = new XmlSerializer(typeof(UISerializedDataList));
            var stream = new FileStream("Assets/Resources/UI/" + "testUI.xml", FileMode.Open);
            var reader = new XmlTextReader(stream);
            UISerializedDataList ui = xml.Deserialize(reader) as UISerializedDataList;
            stream.Close();

            return ui;
        }

        public static UISerializedDataList LoadUIDataFromXml(TextAsset data)
        {
            XmlSerializer xml = new XmlSerializer(typeof(UISerializedDataList));
            var reader = new StringReader(data.text);
            UISerializedDataList ui = xml.Deserialize(reader) as UISerializedDataList;

            return ui;
        }

        public static UISerializedData LoadUIDataFromByte(byte[] data)
        {
            XmlSerializer xml = new XmlSerializer(typeof(UISerializedData));
            UISerializedData x = null;

            using (MemoryStream stream = new MemoryStream(data))
            {
                x = (UISerializedData)xml.Deserialize(stream);
            }

            return x;
        }

        public static List<UITemplateBase> Insts(UISerializedDataList list, Transform pr)
        {
            List<UITemplateBase> objList = new List<UITemplateBase>();

            foreach(var child in list.list)
            {
                var obj = InstiateUI(child, pr);
                
                objList.Add(obj.GetComponent<UITemplateBase>());
            }

            return objList;
        }

		public static HashSet<int> UpdateInsts(UISerializedDataList list, Transform pr) {
			Dictionary<int, UITemplateBase> currentUIDs = new Dictionary<int, UITemplateBase> ();
			HashSet<int> newUIDs = new HashSet<int> ();

			var baseList = pr.gameObject.GetComponentsInChildren<UITemplateBase>();
			foreach(var ui in baseList) {
				currentUIDs.Add (ui.uid, ui);
			}

			foreach (var data in list.list) {
				newUIDs.Add (data.uid);

				if (!currentUIDs.ContainsKey (data.uid)) {
					InstiateUI (data, pr);
				} else {
					var ui = currentUIDs [data.uid];
					data.ApplyTo (ui);
					ui.ApplyUIData ();
				}
			}

			return newUIDs;
		}

		public static void UpdateUIStateList(UISerializedDataList list, Dictionary<int, UIState> states) {
			HashSet<int> currentUIDs = new HashSet<int> ();
			HashSet<int> newUIDs = new HashSet<int> ();
			List<UISerializedData> datas = new List<UISerializedData> ();

			foreach (var key in states.Keys) {
				currentUIDs.Add (key);
			}
			foreach (var uie in list.list) {
				datas.Add (uie);
				newUIDs.Add (uie.uid);
			}

			foreach (var uid in states.Keys) {
				if (!newUIDs.Contains (uid)) {
					states.Remove (uid);
				}
			}
			foreach (var data in datas) {
				if (!currentUIDs.Contains (data.uid)) {
					states.Add (data.uid, ConvertToStateScript (data.type));
				}
			}
		}

        public static Dictionary<int, UIState> GetUIStateList(UISerializedDataList list)
        {
            var dic = new Dictionary<int, UIState>();

            foreach(var d in list.list)
            {
                int a = d.uid;
                var st = ConvertToStateScript(d.type);
				if (st != null) {
					st.uid = a;

					dic.Add (a, st);
				}
            }

            return dic;
        }

		public static Dictionary<string, UIState> GetTagMap(UISerializedDataList list, Dictionary<int, UIState> states)
		{
			var dic = new Dictionary<string, UIState>();

			foreach(var d in list.list)
			{
				if (!string.IsNullOrEmpty (d.tag)) {
					dic.Add (d.tag, states [d.uid]);
				}
			}

			return dic;
		}

        public static GameObject InstiateUI(UISerializedData data, Transform pr)
        {
            if (dic.ContainsKey(data.type))
            {
                var str = dic[data.type];
                var rsc = Resources.Load<GameObject>("Prefabs/" + str);

                if (rsc != null)
                {
                    var obj = GameObject.Instantiate(rsc, pr);
					data.ApplyTo (obj.GetComponent<UITemplateBase>());
                    
                    return obj;
                }
            }
            return null;
        }

        static UIState ConvertToStateScript(UIType type)
        {
            switch (type)
            {
                case UIType.ClickButton:
                    return new ClickButtonState();
                case UIType.OneAxisSlider:
                    return new OneAxisState();
                case UIType.TwoAxis:
                    return new TwoAxisState();
                case UIType.OneAxisJoystic:
					return new OneAxisState();
				case UIType.OneAxisWheel:
					return new OneAxisState();
                default:
                    return null;
            }

        }
    }
}