﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ACS.UI {
	public class OneAxisScript : UITemplateBase
	{
		public Slider slider;
		public RawImage img_handle;
		public RawImage img_background;
		public RawImage img_fill;
		OneAxisState state;

		[SerializeField] public OneAxisUIData uiData = new OneAxisUIData ();

		private void Awake()
		{
			state = new OneAxisState ();
		}

		public OneAxisScript() : base()
		{
			type = UIType.OneAxisSlider;
		}

		public void ChangeValue(float newValue) {
			if (state.GetFloat() != newValue) {
				state.ChangeFloat (newValue);
				SyncState ();
			}
		}

		public override string GetMessage()
		{
			return UIStateParser.Instance.Encode (this.uid, state.Encode ());
		}

		public void DragEnd() {
			if (uiData.autoReset) {
				slider.value = uiData.autoResetValue;
			}
		}

		public override void ApplyUIData() {
			slider.minValue = uiData.min;
			slider.maxValue = uiData.max;

			UIXMLParser parser = UIXMLParser.Instance;
			parser.DecodeRawImage (img_handle, uiData.img_handle);
			parser.DecodeRawImage (img_background, uiData.img_background);
			parser.DecodeRawImage (img_fill, uiData.img_fill);

            slider.handleRect.sizeDelta = uiData.wh;
        }

		public override void UpdateUIData ()
		{
			UIXMLParser parser = UIXMLParser.Instance;
			uiData.img_handle = parser.EncodeRawImage (img_handle);
			uiData.img_background = parser.EncodeRawImage (img_background);
			uiData.img_fill = parser.EncodeRawImage (img_fill);

            uiData.wh = slider.handleRect.sizeDelta;
        }
	}
}
