﻿using System;
using System.Xml;
using System.Xml.Serialization;

using UnityEngine;

namespace ACS.UI
{
	[Serializable]
	public class OneAxisUIData: UIDataAdditional
	{
		[XmlElement] public float min;
		[XmlElement] public float max;
		[XmlElement] public bool autoReset;
		[XmlElement] public float autoResetValue;

		[XmlElement] public string img_handle;
		[XmlElement] public string img_background;
		[XmlElement] public string img_fill;

        [XmlElement] public Vector2 wh;
    }
}

