﻿using System;
using System.Xml;
using System.Xml.Serialization;

namespace ACS.UI
{
	[Serializable]
	public class ClickButtonUIData: UIDataAdditional
	{
		[XmlElement] public string img;
	}
}
