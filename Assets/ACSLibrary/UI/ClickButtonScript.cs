﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ACS.UI {
    public class ClickButtonScript : UITemplateBase
    {
		public RawImage img;
		ClickButtonState state;

		[SerializeField] public ClickButtonUIData uiData = new ClickButtonUIData ();

        private void Awake()
        {
			state = new ClickButtonState ();
        }

        public ClickButtonScript() : base()
        {
            type = UIType.ClickButton;
		}

		public void ChangeValue(bool value) {
			if (state.GetBool() != value) {
				state.ChangeBool (value);
				SyncState ();
			}
		}

        public override string GetMessage()
        {
			return UIStateParser.Instance.Encode (this.uid, state.Encode());
        }

		public override void ApplyUIData ()
		{
			UIXMLParser.Instance.DecodeRawImage (img, uiData.img);
		}

		public override void UpdateUIData ()
		{
			uiData.img = UIXMLParser.Instance.EncodeRawImage (img);
		}
    }
}
