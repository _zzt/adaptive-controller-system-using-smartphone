﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ACS.UI
{
    public abstract class UIState
    {
        public bool isActive = false;
        public int uid;

        public abstract void ChangeUIState(string s);

		public virtual bool GetBool() {
			return false;
		}

		public virtual float GetFloat() {
			return 0;
		}

		public virtual Vector2 GetVector2() {
			return Vector2.zero;
		}

		public abstract string Encode ();
    }
}