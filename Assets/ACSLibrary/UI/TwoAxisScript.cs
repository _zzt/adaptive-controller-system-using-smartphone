﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ACS.UI {
	public class TwoAxisScript : UITemplateBase
	{
		public SingleJoystick joystick;
		public RawImage img_handle;
		public RawImage img_background;
		public RectTransform handle;
		TwoAxisState state;

		[SerializeField] public TwoAxisUIData uiData = new TwoAxisUIData ();

		private void Awake()
		{
			state = new TwoAxisState ();
		}

		public TwoAxisScript() : base()
		{
			type = UIType.TwoAxis;
		}

		public override string GetMessage()
		{
			return UIStateParser.Instance.Encode (this.uid, state.Encode ());
		}

		void Update() {
			Vector3 raw = joystick.GetInputDirection ();
			Vector2 newValue = new Vector2 (raw.x, raw.y);
			if (state.GetVector2 () != newValue) {
				state.ChangeVector2 (newValue);
				SyncState ();
			}
		}

		public override void ApplyUIData ()
		{
			UIXMLParser parser = UIXMLParser.Instance;
			parser.DecodeRawImage (img_handle, uiData.img_handle);
			parser.DecodeRawImage (img_background, uiData.img_background);

			handle.sizeDelta = new Vector2 (uiData.width, uiData.height);
			//joystick.Initialize ();
		}

		public override void UpdateUIData ()
		{
			UIXMLParser parser = UIXMLParser.Instance;
			uiData.img_handle = parser.EncodeRawImage (img_handle);
			uiData.img_background = parser.EncodeRawImage (img_background);
			uiData.width = handle.sizeDelta.x;
			uiData.height = handle.sizeDelta.y;
		}
	}
}
