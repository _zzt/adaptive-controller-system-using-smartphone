﻿using System;
using System.Xml;
using System.Xml.Serialization;

namespace ACS.UI
{
	[Serializable]
	public class ImageUIData: UIDataAdditional
	{
		[XmlElement] public string img;
	}
}
