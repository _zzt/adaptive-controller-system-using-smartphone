﻿using System;
using System.Collections;
using System.IO;
using System.Xml.Serialization;

using UnityEngine.UI;
using UnityEngine;

namespace ACS.UI {
	public class UIXMLParser {
		private static object _lock = new object ();

		private static volatile UIXMLParser _instance;
		public static UIXMLParser Instance {
			get {
				if (_instance == null) {
					lock (_lock) {
						if (_instance == null)
							_instance = new UIXMLParser ();
					}
				}
				return _instance;
			}
		}

		public byte[] Encode(UISerializedDataList list) {
			// not needed????
			return null;
		}

		public UISerializedDataList Decode(byte[] data) {
			var xml = new XmlSerializer(typeof(UISerializedDataList));
			var ms = new MemoryStream (data, false);

			UISerializedDataList list = null;
			try {
				list = (UISerializedDataList) xml.Deserialize (ms);
			} catch (InvalidOperationException e) {} 

			return list;
		}

		public string EncodeRawImage(RawImage img) {
            Texture2D tex;

            if(img.texture != null)
            {
                tex = (Texture2D)img.texture;
            }
            else
            {
                tex = Resources.Load<Texture2D>("emptyBG");
            }

            tex = DeCompress(tex);

			byte[] pngEncodedTexture = ImageConversion.EncodeToPNG (tex);
			return Convert.ToBase64String (pngEncodedTexture);
		}

		public void DecodeRawImage(RawImage img, string data) {
			Texture2D tex = (Texture2D)(img.texture == null ? new Texture2D (1, 1) : img.texture);
			if (!string.IsNullOrEmpty (data)) {
				byte[] pngEncodedTexture = Convert.FromBase64String (data);
				ImageConversion.LoadImage (tex, pngEncodedTexture);
			}
			img.texture = tex;
		}

        // 2018.2.0f2 에서 발견, 압축된 이미지이면 EncodeToPNG 가 안됨.
        public static Texture2D DeCompress(Texture2D source)
        {
            RenderTexture renderTex = RenderTexture.GetTemporary(
                        source.width,
                        source.height,
                        0,
                        RenderTextureFormat.Default,
                        RenderTextureReadWrite.Linear);

            Graphics.Blit(source, renderTex);
            RenderTexture previous = RenderTexture.active;
            RenderTexture.active = renderTex;
            Texture2D readableText = new Texture2D(source.width, source.height);
            readableText.ReadPixels(new Rect(0, 0, renderTex.width, renderTex.height), 0, 0);
            readableText.Apply();
            RenderTexture.active = previous;
            RenderTexture.ReleaseTemporary(renderTex);
            return readableText;
        }
    }
}
