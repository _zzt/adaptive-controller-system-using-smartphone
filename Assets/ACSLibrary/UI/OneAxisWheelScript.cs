﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace ACS.UI {
	public class OneAxisWheelScript : UITemplateBase
	{
		public Wheel wheel;
		public RawImage img_handle;

		public RectTransform handle;
		OneAxisState state;

		[SerializeField] public OneAxisWheelUIData uiData = new OneAxisWheelUIData();

		private void Awake()
		{
			state = new OneAxisState();
		}

		public OneAxisWheelScript() : base()
		{
			type = UIType.OneAxisWheel;
		}

		public void ChangeValue(float newValue)
		{
			if (state.GetFloat() != newValue)
			{
				state.ChangeFloat(newValue);
				SyncState();
			}
		}

		public override string GetMessage()
		{
			return UIStateParser.Instance.Encode(this.uid, state.Encode());
		}

		private void Update()
		{
			float newValue = wheel.GetRotationInDegree();
			if (state.GetFloat() != newValue)
			{
				state.ChangeFloat(newValue);
				SyncState();
			}
		}


		public override void ApplyUIData()
		{
			UIXMLParser parser = UIXMLParser.Instance;
			parser.DecodeRawImage(img_handle, uiData.img_handle);
			handle.sizeDelta = new Vector2 (uiData.handle_w, uiData.handle_h);
			Debug.Log (handle.rect.width);
			//handle.rect.Set (0, 0, uiData.handle_w, uiData.handle_h);
			wheel.SetAutoReset (uiData.autoReset, uiData.autoResetValue);
			if (uiData.maxExist)
				wheel.maxVal = uiData.max_deg;
			if (uiData.minExist)
				wheel.minVal = uiData.min_deg;
		}

		public override void UpdateUIData()
		{
			UIXMLParser parser = UIXMLParser.Instance;
			uiData.img_handle = parser.EncodeRawImage(img_handle);
			uiData.handle_w = handle.rect.width;
			uiData.handle_h = handle.rect.height;

			uiData.autoReset = wheel.autoReset;
			uiData.autoResetValue = wheel.autoResetVal;
			uiData.maxExist = !float.IsPositiveInfinity (wheel.maxVal);
			uiData.max_deg = wheel.maxVal;
			uiData.minExist = !float.IsNegativeInfinity (wheel.minVal);
			uiData.min_deg = wheel.minVal;
		}
	}
}
