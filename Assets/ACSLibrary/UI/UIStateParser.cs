﻿using System;

namespace ACS.UI
{
	public class UIStateParser
	{
		private static object _lock = new object ();

		private static volatile UIStateParser _instance;
		public static UIStateParser Instance {
			get {
				if (_instance == null) {
					lock (_lock) {
						if (_instance == null)
							_instance = new UIStateParser ();
					}
				}
				return _instance;
			}
		}

		public UIStateParseResult Decode(string data) {
			var splitdata = data.Split(':');

			var result = new UIStateParseResult();
			if (splitdata.Length == 2) {
				result.succeed = true;
				result.uid = int.Parse (splitdata [0]);
				result.message = splitdata [1];
			}
			return result;
		}

		public string Encode(int uid, string message) {
			return uid.ToString() + ":" + message;
		}
	}

	public struct UIStateParseResult {
		public bool succeed;
		public int uid;
		public string message;
	}
}

