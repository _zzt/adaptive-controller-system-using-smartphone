﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ACS.UI
{
    public class OneAxisJoysticScript : UITemplateBase
    {
        public OneAxisJoystic axis;
        public RawImage img_handle;
        public RawImage img_background;

        public RectTransform handle;
        OneAxisState state;

        [SerializeField] public TwoAxisUIData uiData = new TwoAxisUIData();

        private void Awake()
        {
            state = new OneAxisState();
        }

        public OneAxisJoysticScript() : base()
        {
            type = UIType.OneAxisSlider;
        }

        public void ChangeValue(float newValue)
        {
            if (state.GetFloat() != newValue)
            {
                state.ChangeFloat(newValue);
                SyncState();
            }
        }

        public override string GetMessage()
        {
            return UIStateParser.Instance.Encode(this.uid, state.Encode());
        }

        private void Update()
        {
            Vector3 raw = axis.GetInputDirection();
            float newValue = raw.x;
            if (state.GetFloat() != newValue)
            {
                state.ChangeFloat(newValue);
                SyncState();
            }
        }


        public override void ApplyUIData()
        {
            UIXMLParser parser = UIXMLParser.Instance;
            parser.DecodeRawImage(img_handle, uiData.img_handle);
            parser.DecodeRawImage(img_background, uiData.img_background);

            handle.rect.Set(0, 0, uiData.width, uiData.height);
        }

        public override void UpdateUIData()
        {
            UIXMLParser parser = UIXMLParser.Instance;
            uiData.img_handle = parser.EncodeRawImage(img_handle);
            uiData.img_background = parser.EncodeRawImage(img_background);

            uiData.width = handle.rect.width;
            uiData.height = handle.rect.height;
        }
    }
}
