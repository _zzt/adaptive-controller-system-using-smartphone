﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ACS.UI
{
	public class TwoAxisState : UIState
	{
		Vector2 _value;

		public void ChangeVector2(Vector2 value) {
			_value = value;
		}

		public override void ChangeUIState(string s)
		{
			string[] split = s.Split (',');
			_value = new Vector2 (float.Parse (split [0]), float.Parse (split [1]));
		}

		public override Vector2 GetVector2 ()
		{
			return _value;
		}

		public override string Encode ()
		{
			return string.Format ("{0},{1}", _value.x.ToString (), _value.y.ToString ());
		}
	}
}
