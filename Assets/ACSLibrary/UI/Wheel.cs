﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

namespace ACS.UI {
	public class Wheel : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
	{
		public float maxVal = float.PositiveInfinity;
		public float minVal = float.NegativeInfinity;

		public bool autoReset = false;
		public float autoResetVal = 0;

		float value;

		RawImage handleImage;

		float initialDeg;
		Vector2 initialPointer;
		Vector2 prevPointer;

		public void SetRotation(float deg) {
			value = deg;
			handleImage.rectTransform.localRotation = Quaternion.AngleAxis (deg, handleImage.rectTransform.forward);
		}

		public void SetAutoReset(bool autoReset, float autoResetVal) {
			this.autoReset = autoReset;
			this.autoResetVal = autoResetVal;
		}

		public float GetRotationInDegree() {
			return value;
		}

		void Start() {
			handleImage = GetComponentInChildren<RawImage> ();
		}

		// this event happens when there is a drag on screen
		public virtual void OnDrag(PointerEventData ped)
		{
			Vector2 pointer = GetLocalPointer (ped);
			if (pointer.magnitude > 0.3f) {
				Rotate (pointer);
			}
		}

		// this event happens when there is a touch down (or mouse pointer down) on the screen
		public virtual void OnPointerDown(PointerEventData ped)
		{
			initialDeg = GetRotationInDegree ();
			initialPointer = GetLocalPointer (ped);
		}

		// this event happens when the touch (or mouse pointer) comes up and off the screen
		public virtual void OnPointerUp(PointerEventData ped)
		{
			if (autoReset) {
				SetRotation (autoResetVal);
			}
		}

		Vector2 GetLocalPointer(PointerEventData ped) {
			Vector2 localPoint = Vector2.zero; // resets the localPoint out parameter of the RectTransformUtility.ScreenPointToLocalPointInRectangle function on each drag event

			// if the point touched on the screen is within the background image of this joystick
			RectTransform rect = GetComponent<RectTransform>();
			if (RectTransformUtility.ScreenPointToLocalPointInRectangle (rect, ped.position, ped.pressEventCamera, out localPoint)) {
				localPoint.x = (localPoint.x / rect.sizeDelta.x);
				localPoint.y = (localPoint.y / rect.sizeDelta.y);
				//localPoint = new Vector2(localPoint.x * 2 + 1, localPoint.y * 2 - 1);
			}
			return localPoint;
		}

		void Rotate(Vector2 pointer) {
			float currentDeg = GetRotationInDegree ();
			float deltaDeg = Vector2.SignedAngle (prevPointer, pointer);
			float inaccurateDeg = currentDeg + deltaDeg;

			int rev = Mathf.RoundToInt (inaccurateDeg / 360);

			float normalizedDeltaDeg = Vector2.SignedAngle (initialPointer, pointer);
			float accurateNormalizedDeg = Mathf.DeltaAngle (0, initialDeg + normalizedDeltaDeg);
			float accurateDeg = accurateNormalizedDeg + 360 * rev;

			float clampedDeg = Mathf.Clamp (accurateDeg, minVal, maxVal);
			SetRotation (clampedDeg);

			if (clampedDeg != accurateDeg) {
				initialDeg = GetRotationInDegree ();
				initialPointer = pointer;
			}
			prevPointer = pointer;
		}
	}
}
