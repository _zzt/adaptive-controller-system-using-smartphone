﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using ACS.UI;

namespace ACS.Library
{
    public class Controller
    {
        public int index;
        public int connectionId;

        public bool isEnable = false;

        public Dictionary<int, UIState> states = new Dictionary<int, UIState>();
		public Dictionary<string, UIState> tagMap = new Dictionary<string, UIState>();

		public bool gyroEnabled = false;
		public Quaternion gyro = Quaternion.identity;

        public Controller()
        {

        }

		public void InitializeState(UISerializedDataList list) {
			states.Clear ();

			//states = UISerializer.GetUIStateList (list);
			UISerializer.UpdateUIStateList(list, states);
			tagMap = UISerializer.GetTagMap (list, states);
		}

		public void UpdateUIState(int uid, string data)
        {
			if (uid < 0)
				UpdateGyro (data);
			else
				states [uid].ChangeUIState (data);
        }

		public UIState GetState(int uid) {
			UIState result;
			if (states.TryGetValue (uid, out result)) {
				return result;
			}
			return null;
		}

		public UIState GetState(string tag) {
			UIState result;
			if (tagMap.TryGetValue (tag, out result)) {
				return result;
			}
			return null;
		}

		public Quaternion GetGyro() {
			return gyro;
		}

		public void SendUI(TextAsset ui) {
			Sender.Instance.Send (this, ui);
		}

		public void SetGyro(bool enabled) {
			gyroEnabled = enabled;
			if (!gyroEnabled)
				gyro = Quaternion.identity;
			
			string str = string.Format ("-1:{0}", enabled);
			Sender.Instance.Send(this, str);
		}

        /// <summary>
        /// 지금은 이미지만 됨 ㅋ
        /// </summary>
        public void SetUIActive(int uid, bool b)
        {
            string str = string.Format("{0}:{1}", uid, b);
            Sender.Instance.Send(this, str);
        }

        public void SetUIActive(string str, bool b)
        {
            SetUIActive(tagMap[str].uid, b);
        }

		public void Vibrate() {
			string str = "-2:true";
			Sender.Instance.Send (this, str);
		}

		void UpdateGyro(string data) {
			var split = data.Split (',');
			if (split.Length != 4)
				return;

			gyro.x = float.Parse (split [0]);
			gyro.y = float.Parse (split [1]);
			gyro.z = float.Parse (split [2]);
			gyro.w = float.Parse (split [3]);
		}
    }
}

