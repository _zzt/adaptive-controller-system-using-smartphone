﻿using System.Net;
using System.Collections;
using System.Collections.Generic;

namespace ACS.Library {
	public class NetworkInspector {

		private static object _lock = new object ();

		private static volatile NetworkInspector _instance;
		public static NetworkInspector Instance {
			get {
				if (_instance == null) {
					lock (_lock) {
						if (_instance == null)
							_instance = new NetworkInspector ();
					}
				}
				return _instance;
			}
		}

		public ServerInformation GetServerInfo() {
			var info = new ServerInformation ();
			info.LocalIP = GetLocalIP ();

			return info;
		}

		public string GetLocalIP() {
			string hostName = System.Net.Dns.GetHostName ();
			IPHostEntry ipEntry = System.Net.Dns.GetHostEntry (hostName);
			IPAddress[] addr = ipEntry.AddressList;

			return addr [addr.Length - 1].ToString ();
		}
	}
}