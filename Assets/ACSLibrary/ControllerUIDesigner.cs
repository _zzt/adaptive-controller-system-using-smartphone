﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

using ACS.UI;

namespace ACS.Library {
	public class ControllerUIDesigner : MonoBehaviour {

		public static int index = 0;

		[SerializeField]
		public Canvas canvas;

		[SerializeField]
		public string filename;

		[SerializeField]
		public TextAsset loadXML;

		public UISerializedDataList GetUIListFromCanvas() {
			UISerializedDataList result = new UISerializedDataList();

			var baseList = canvas.gameObject.GetComponentsInChildren<UITemplateBase>();
			foreach (var child in baseList)
			{
				child.UpdateUIData ();
				UISerializedData UIcomponent = new UISerializedData(child);
				UIcomponent.uid = child.uid == 0? ++index : child.uid;

				result.list.Add(UIcomponent);
			}

			return result;
		}
	}
}