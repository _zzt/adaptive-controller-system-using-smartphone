﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

using ACS;
using UnityEngine.UI;
using System;

namespace ACS.Library {
	public class ConnectionManager : MonoBehaviour {

        private static volatile ConnectionManager _instance;
        public static ConnectionManager Instance
        {
            get
            {
                Debug.Assert(_instance != null);
                return _instance;
            }
        }

        private void Awake()
        {
            Debug.Assert(_instance == null);
            _instance = this;
        }

        public bool isRunning = false;
		public bool isOpen = false;

		void OnEnable () {
			DontDestroyOnLoad (this);
			isRunning = false;
			isOpen = false;
		}

		void OnDisable () {
			Close ();
			Stop ();
		}

		public void Run () {
			NetworkServer.dontListen = true;

			NetworkServer.RegisterHandler (MsgType.Connect, OnConnected);
			NetworkServer.RegisterHandler (MsgType.Disconnect, OnDisconnected);
			NetworkServer.RegisterHandler (ConnectionConfiguration.ClientMessageType, OnMessageIncoming);
			isRunning = true;
		}

		public void Stop () {
			NetworkServer.Shutdown ();
			isRunning = false;

			NetworkServer.Reset ();
		}

		public void Open () {
			NetworkServer.dontListen = false;
			NetworkServer.Listen (ConnectionConfiguration.Port);
			isOpen = true;
		}

		public void Close () {
			NetworkServer.dontListen = true;
			isOpen = false;
		}

		public void Send (int connectionId, byte[] data) {
			ByteBase bytebase = new ByteBase (data);
			NetworkServer.SendToClient (connectionId, ConnectionConfiguration.ServerMessageType, bytebase);
		}

		public delegate void ConnectionVoidEvent (int connectionId);
		public delegate void ConnectionMessageEvent (int connectionId, byte[] data);
		public event ConnectionVoidEvent Connected;
		public event ConnectionVoidEvent Disconnected;
		public event ConnectionMessageEvent MessageReceived;

		private void OnConnected(NetworkMessage msg) {
			int id = msg.conn.connectionId;
			if (Connected != null)
				Connected (id);
		}

		private void OnDisconnected(NetworkMessage msg) {
			int id = msg.conn.connectionId;
			if (Disconnected != null)
				Disconnected (id);
		}

		private void OnMessageIncoming(NetworkMessage msg) {
			int id = msg.conn.connectionId;
			ByteBase bytebase = new ByteBase (null);
			bytebase.Deserialize (msg.reader);
			byte[] data = bytebase.data;
			if (MessageReceived != null)
				MessageReceived (id, data);
		}
	}
}