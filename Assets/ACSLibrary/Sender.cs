﻿using UnityEngine;

using System;
using System.Collections;
using System.Text;

using ACS.UI;

namespace ACS.Library {
	public class Sender : MonoBehaviour
	{
		private static volatile Sender _instance;
		public static Sender Instance
		{
			get
			{
				return _instance;
			}
		}

		void Awake() {
			if (_instance != null) {
				Destroy (this);
			}
			_instance = this;
		}

		public ConnectionManager conn;
		public ControllerManager controllers;

		void Start ()
		{
			conn = conn ?? ConnectionManager.Instance;
			controllers = controllers ?? ControllerManager.Instance;

			Debug.Assert (conn != null && controllers != null);
		}

		public void Send(Controller con, string str) {
			Send(con, Encoding.Default.GetBytes(str));
		}

		public void Send(Controller con, byte[] data) {
			// For testing only
			conn.Send (con.connectionId, data);
		}

		public void Send(Controller con, TextAsset xml) {
			byte[] data = xml.bytes;
			var list = UIXMLParser.Instance.Decode (data);
			con.InitializeState (list);

			int bytesLeft = data.Length;
			Send (con, string.Format ("0:{0}", bytesLeft));
			StartCoroutine (SendSegments (bytesLeft, data, con));
		}

		IEnumerator SendSegments(int bytesLeft, byte[] data, Controller con) {
			int index = 0;
			int packetsPerFrame = 0;
			while (bytesLeft > 0) {
				int currentPacketSize = Mathf.Min (bytesLeft, 64000);
				if (bytesLeft - currentPacketSize < 100)
					currentPacketSize = bytesLeft;

				byte[] packetData = new byte[currentPacketSize];
				Array.Copy (data, index, packetData, 0, currentPacketSize);
				conn.Send (con.connectionId, packetData);
				index += currentPacketSize;
				bytesLeft -= currentPacketSize;
				yield return null;
			}
		}
	}
}
