﻿using System.Collections;

namespace ACS {
	public class QRCodeDataProcessor
	{
		private static object _lock = new object ();

		private static volatile QRCodeDataProcessor _instance;
		public static QRCodeDataProcessor Instance {
			get {
				if (_instance == null) {
					lock (_lock) {
						if (_instance == null)
							_instance = new QRCodeDataProcessor ();
					}
				}
				return _instance;
			}
		}

		public string Serialize(ServerInformation info) {
			return info.LocalIP;
		}

		public string Compress(string data) {
			return data;
		}

		public string Decompress(string compressedData) {
			return compressedData;
		}

		public ServerInformation Deserialize(string data) {
			var info = new ServerInformation ();
			info.LocalIP = data;
			return info;
		}
	}
}