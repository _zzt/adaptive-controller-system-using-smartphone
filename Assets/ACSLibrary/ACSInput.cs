﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using ACS.UI;

namespace ACS.Library {
	public class ACSInput : MonoBehaviour
	{

		public ControllerManager controllers;

		private static volatile ACSInput _instance;
		public static ACSInput Instance
		{
			get
			{
				return _instance;
			}
		}

		void Awake() {
			if (_instance != null) {
				Destroy (this);
			}
			_instance = this;
		}

		void Start() {
			
			StartCoroutine (UpdatePrev ());
		}

		public bool GetButtonDown(int controllerIndex, string tag) {
			return GetButtonDown (controllers.Get (controllerIndex), tag);
		}

		public bool GetButtonDown(Controller controller, string tag) {
			ClickButtonState state = (ClickButtonState)controller.GetState (tag);
			bool result = state.curr && !state.prev;
			if (result)
				state.prev = state.curr;
			return result;
		}

		public bool GetButtonUp(int controllerIndex, string tag) {
			return GetButtonUp (controllers.Get (controllerIndex), tag);
		}

		public bool GetButtonUp(Controller controller, string tag) {
			ClickButtonState state = (ClickButtonState)controller.GetState (tag);
			bool result = !state.curr && state.prev;
			if (result)
				state.prev = state.curr;
			return !state.curr && state.prev;
		}

		public bool GetButton(int controllerIndex, string tag) {
			return GetButton (controllers.Get (controllerIndex), tag);
		}

		public bool GetButton(Controller controller, string tag) {
			ClickButtonState state = (ClickButtonState)controller.GetState (tag);
			return state.GetBool ();
		}

		public float GetAxis1D(int controllerIndex, string tag) {
			return GetAxis1D (controllers.Get (controllerIndex), tag);
		}

		public float GetAxis1D(Controller controller, string tag) {
			OneAxisState state = (OneAxisState)controller.GetState (tag);
			return state.GetFloat ();
		}

		public Vector2 GetAxis2D(int controllerIndex, string tag) {
			return GetAxis2D (controllers.Get (controllerIndex), tag);
		}

		public Vector2 GetAxis2D(Controller controller, string tag) {
			TwoAxisState state = (TwoAxisState)controller.GetState (tag);
			return state.GetVector2 ();
		}

		IEnumerator UpdatePrev () {
			while (true) {
				yield return new WaitForEndOfFrame ();
				yield return new WaitForEndOfFrame ();
				yield return new WaitForEndOfFrame ();
				yield return new WaitForEndOfFrame ();
				yield return new WaitForEndOfFrame ();
				UpdatePrevStep ();
			}
		}

		void UpdatePrevStep() {
			foreach (var controller in controllers.controllers) {
				if (controller == null)
					continue;
				foreach (UIState state in controller.states.Values) {
					if (state is ClickButtonState) {
						ClickButtonState boolState = (ClickButtonState)state;
						boolState.prev = boolState.curr;
					}
				}
			}
		}
	}
}
