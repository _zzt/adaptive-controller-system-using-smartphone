﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using ACS.UI;

namespace ACS.Library {
	public class Listener : MonoBehaviour
	{
		private static volatile Listener _instance;
		public static Listener Instance
		{
			get
			{
				return _instance;
			}
		}

		void Awake() {
			if (_instance != null) {
				Destroy (this);
			}
			_instance = this;
		}

		public ConnectionManager conn;
		public ControllerManager controllers;

		public delegate void ListenerVoidEvent(Controller con);
		public delegate void ListenerDataEvent(Controller con, byte[] data);

		public event ListenerVoidEvent Connected;
		public event ListenerVoidEvent Disconnected;
		public event ListenerDataEvent MessageReceived;

		void Start ()
		{
			conn = conn ?? ConnectionManager.Instance;
			controllers = controllers ?? ControllerManager.Instance;

			Debug.Assert (conn != null && controllers != null);

			conn.Connected += AddNewController;
			conn.Disconnected += RemoveController;
			conn.MessageReceived += RelayMessage;

            MessageReceived += ReceiveUIMessage;

        }

		void OnDestroy() {
			conn.Connected -= AddNewController;
			conn.Disconnected -= RemoveController;
			conn.MessageReceived -= RelayMessage;

            MessageReceived -= ReceiveUIMessage;

        }

		void AddNewController (int connectionId)
		{
			Controller con = new Controller ();
			con.connectionId = connectionId;
			controllers.Add (con);

			if (Connected != null) {
				Connected (con);
			}
		}

		void RemoveController (int connectionId)
		{
			Controller con = controllers.LookupConnectionId (connectionId);
			Debug.Assert (con != null);
			controllers.RemoveAt (con.index);

			if (Disconnected != null) {
				Disconnected (con);
			}
		}

        void RelayMessage (int connectionId, byte[] data)
		{
			Controller con = controllers.LookupConnectionId (connectionId);
			if (MessageReceived != null) {
				MessageReceived (con, data);
			}
		}

        void ReceiveUIMessage(Controller con, byte[] data)
        {
            string check = Encoding.Default.GetString(data);
			UIStateParseResult result = UIStateParser.Instance.Decode (check);
			if (result.succeed)
				con.UpdateUIState (result.uid, result.message);
        }
    }
}