﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

using ACS.UI;

namespace ACS.Library {
	[CustomEditor(typeof(ControllerUIDesigner))]
	public class ControllerUIDesignerEditor : Editor {

		SerializedProperty canvas;
		SerializedProperty filename;
		SerializedProperty loadXML;

		void OnEnable() {
			canvas = serializedObject.FindProperty ("canvas");
			filename = serializedObject.FindProperty ("filename");
			loadXML = serializedObject.FindProperty ("loadXML");
		}

		public override void OnInspectorGUI ()
		{
			serializedObject.Update ();
			EditorGUILayout.PropertyField (canvas);
			EditorGUILayout.PropertyField (filename);
			if (GUILayout.Button ("Export")) {
				ExportButtonClicked ();
			}

			EditorGUILayout.PropertyField (loadXML);
			if (GUILayout.Button ("Load")) {
				LoadButtonClicked ();
			}
			serializedObject.ApplyModifiedProperties ();
		}

		void ExportButtonClicked() {
			ControllerUIDesigner myTarget = (ControllerUIDesigner)target;

			if (string.IsNullOrEmpty (myTarget.filename)) {
				Debug.LogError ("Filename cannot be empty!");
				return;
			}

			Debug.Log ("Exporting GUI...");
			UISerializedDataList list = myTarget.GetUIListFromCanvas ();
			string path = Application.dataPath + Path.DirectorySeparatorChar + myTarget.filename + ".xml";
			UISerializer.SaveUIDataToXml (list, path);
			AssetDatabase.Refresh ();
			Debug.Log ("Exporting complete!");
		}

		void LoadButtonClicked() {
			ControllerUIDesigner myTarget = (ControllerUIDesigner)target;

			Debug.Log ("Loading GUI...");
			var list = UISerializer.LoadUIDataFromXml (myTarget.loadXML);
			if (list == null) {
				Debug.LogError ("Ill-formatted file!");
			}
			//ClearCanvas ();
			UISerializer.UpdateInsts (list, myTarget.canvas.transform);
			Debug.Log ("Loading complete!");
		}

		void ClearCanvas() {
			ControllerUIDesigner myTarget = (ControllerUIDesigner)target;

			foreach (Transform child in myTarget.canvas.transform) {
				DestroyImmediate (child.gameObject);
			}
		}
	}
}