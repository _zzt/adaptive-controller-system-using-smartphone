﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using ZXing;
using ZXing.QrCode;

using ACS;

namespace ACS.Library {
	public class QRCodeGenerator
	{
		private static object _lock = new object ();

		private static volatile QRCodeGenerator _instance;
		public static QRCodeGenerator Instance {
			get {
				if (_instance == null) {
					lock (_lock) {
						if (_instance == null)
							_instance = new QRCodeGenerator ();
					}
				}
				return _instance;
			}
		}

		public Texture2D Generate() {
			NetworkInspector inspector = NetworkInspector.Instance;
			QRCodeDataProcessor processor = QRCodeDataProcessor.Instance;

			ServerInformation info = inspector.GetServerInfo ();
			string serializedInfo = processor.Serialize (info);
			string compressed = processor.Compress (serializedInfo);

			Color32[] color32 = Encode (compressed);
			Texture2D texture = new Texture2D (256, 256);
			texture.SetPixels32 (color32);
			texture.Apply ();
			return texture;
		}

		private Color32[] Encode(string data) {
			var writer = new BarcodeWriter {
				Format = BarcodeFormat.QR_CODE,
				Options = new QrCodeEncodingOptions {
					Height = 256,
					Width = 256
				}
			};
			return writer.Write (data);
		}
	}
}
