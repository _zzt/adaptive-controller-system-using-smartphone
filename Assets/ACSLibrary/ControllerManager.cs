﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ACS.Library {
	public class ControllerManager : MonoBehaviour {

		private static volatile ControllerManager _instance;
		public static ControllerManager Instance
		{
			get
			{
				return _instance;
			}
		}

		void Awake() {
			if (_instance != null) {
				Destroy (this);
			}
			_instance = this;
		}

		public int maxControllers = 4;
		public Controller[] controllers;

		public ControllerManager() {
			controllers = new Controller[maxControllers];
		}

		public bool Add(Controller controller) {
			for (int i = 0; i < maxControllers; i++) {
				if (controllers [i] == null) {
					controllers [i] = controller;
					controller.index = i;
					return true;
				}
			}
			return false;
		}

		public Controller RemoveAt(int index) {
			Controller item = controllers [index];
			controllers [index] = null;
			return item;
		}

        public Controller Get(int index) {
			return controllers [index];
		}

		public Controller LookupConnectionId(int connectionId) {
			for (int i = 0; i < maxControllers; i++) {
				Controller con = Get (i);
				if (con != null && con.connectionId == connectionId) {
					return con;
				}
			}
			return null;
		}
    }
}
