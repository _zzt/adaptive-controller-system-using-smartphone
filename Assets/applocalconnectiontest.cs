﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ACS.App;
using ACS;
using UnityEngine.SceneManagement;


public class applocalconnectiontest : MonoBehaviour {

	ConnectionManager conn;

	public void ConnectLocal() {
		conn = ConnectionManager.Instance;

		ServerInformation info = new ServerInformation ();
		info.LocalIP = "localhost";
		conn.Connected += OnConnect;
		conn.Initialize (info);
	}

	void OnConnect()
	{
		conn.Connected -= OnConnect;
		SceneManager.LoadScene("ControllerTestScene");
	}
}
