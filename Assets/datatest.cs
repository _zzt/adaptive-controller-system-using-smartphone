﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Text;
using ACS.App;
using System.Xml.Serialization;
using ACS.UI;
using System.Xml;
using System.IO;

public class datatest : MonoBehaviour {

	ConnectionManager connManager;

	public Text textfield;
	public Transform UIRoot;

	// Use this for initialization
	void Start () {
		connManager = FindObjectOfType<ConnectionManager> ();
		connManager.MessageReceived += OnMessageReceived;
	}

	void OnMessageReceived (byte[] data)
	{
		textfield.text = Encoding.UTF8.GetString (data);

		// TODO: separate to Renderer
		var xml = new XmlSerializer(typeof(UISerializedDataList));
		var ms = new MemoryStream (data, false);

		UISerializedDataList list = null;
		bool success = false;
		try {
			list = (UISerializedDataList) xml.Deserialize (ms);
			success = true;
		} catch (InvalidOperationException e) {
			textfield.text = "Invalid XML";
		} finally {
			if (success) {
				textfield.text = "Valid XML";
			}
		}
		if (success) {
		    var uiList = UISerializer.Insts (list, UIRoot);
		}
	}

	public void OnButtonClick(string data) {
		byte[] bytes = Encoding.UTF8.GetBytes (data);
		connManager.Send (bytes);
	}
}
