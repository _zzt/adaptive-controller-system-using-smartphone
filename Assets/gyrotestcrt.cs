﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gyrotestcrt : MonoBehaviour {

	public Color defaultColor = Color.white;
	public Color highlightColor;
	Renderer rdr;
	Quaternion orient = Quaternion.identity;

	void Start() {
		rdr = GetComponent<Renderer> ();
	}

	public void ChangeRotation(Quaternion q) {
		q.z = -q.z;
		q.w = -q.w;

		transform.rotation = Quaternion.Inverse (orient) * q;
	}

	public void ResetRotation(Quaternion q) {
		q.z = -q.z;
		q.w = -q.w;
		orient = q;
	}

	public void ChangeColor(bool highlight) {
		rdr.material.color = highlight ? highlightColor : defaultColor;
	}
}
