﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using ACS.Library;

public class gyrotest : MonoBehaviour {

	public RawImage qrImg;

	public List<gyrotestcrt> crt;
	public TextAsset controller;

	// Use this for initialization
	void Start () {
		ConnectionManager conn = ConnectionManager.Instance;
		conn.Run ();
		conn.Open ();

		qrImg.texture = QRCodeGenerator.Instance.Generate ();

		Listener listener = Listener.Instance;

		listener.Connected += OnConnect;
		listener.Disconnected += OnDisconnect;
	}

	void OnDisconnect (Controller con)
	{
		GetCRT(con).gameObject.SetActive (false);	
	}

	void OnConnect (Controller con)
	{
		GetCRT(con).gameObject.SetActive (true);
		Sender.Instance.Send (con, controller);
		con.SetGyro (true);
	}
	
	// Update is called once per frame
	void Update () {
		foreach (Controller con in ControllerManager.Instance.controllers) {
			if (con == null)
				continue;

			gyrotestcrt c = GetCRT(con);
			Quaternion gyro = con.GetGyro ();

			if (ACSInput.Instance.GetButtonDown (con, "reset")) {
				con.Vibrate ();
				c.ResetRotation (gyro);
			}
			c.ChangeRotation (gyro);
			c.ChangeColor (con.GetState ("highlight").GetBool());
		}
	}

	gyrotestcrt GetCRT(Controller con) {
		return crt [con.connectionId - 1];
	}
}
