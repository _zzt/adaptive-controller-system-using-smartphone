﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using ACS.Library;
using ACS.UI;

public class latencytest : MonoBehaviour {

	public RawImage qrImg;
	public Text text;
	public TextAsset ui;

	// Use this for initialization
	void Start () {
		ConnectionManager conn = ConnectionManager.Instance;
		conn.Run ();
		conn.Open ();

		qrImg.texture = QRCodeGenerator.Instance.Generate ();

		Listener.Instance.Connected += OnControllerConnect;
	}

	void OnControllerConnect (Controller con)
	{
		Sender.Instance.Send (con, ui);
	}
	
	// Update is called once per frame
	void Update () {
		text.text = "";
		foreach (Controller con in ControllerManager.Instance.controllers) {
			if (con == null)
				continue;
			foreach (int uid in con.states.Keys) {
				ClickButtonState state = con.states [uid] as ClickButtonState;
				if (state == null)
					continue;
				if (state.GetBool ()) {
					text.text = con.index + "-" + uid;
				}
			}
		}
	}
}
