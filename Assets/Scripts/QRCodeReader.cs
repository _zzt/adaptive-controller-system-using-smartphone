﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ZXing;
using ZXing.QrCode;

public class QRCodeReader : MonoBehaviour
{
	public float ScanInterval = 0.5f;

    public delegate void QRCodeDataEvent (string result);
	public event QRCodeDataEvent QRCodeFound;

	private WebCamTexture targetTexture;
	private IEnumerator scanCoroutine;

	public void StartScan(WebCamTexture texture) {
		targetTexture = texture;

		scanCoroutine = scan ();
		StartCoroutine (scanCoroutine);
	}

	public void StopScan() {
		StopCoroutine (scanCoroutine);
	}

	IEnumerator scan() {

        if (!targetTexture.isPlaying)
            yield break;

		while (true) {
			try {
				IBarcodeReader barcodeReader = new BarcodeReader ();

				int W = targetTexture.width;
				int H = targetTexture.height;
				int h = (int)(H / 1.5f);
				int w = h;
				int padW = (W - w)/2;
				int padH = (H - h)/2;

				Color[] scanPart = targetTexture.GetPixels(padW, padH, w, h);
				Texture2D scanPartTexture = new Texture2D(w, h);
				scanPartTexture.SetPixels(scanPart);
				scanPartTexture.Apply();
				var result = barcodeReader.Decode (scanPartTexture.GetPixels32(), w, h);
				if (result != null) {
					QRCodeFound (result.Text);
				} else {
					Debug.Log("nothing");
				}
			} catch (Exception e) {
				Debug.LogWarning (e.Message);
			}

			yield return new WaitForSeconds (ScanInterval);
		}
	}
}

