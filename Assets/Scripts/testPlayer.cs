﻿using ACS.Library;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ACS.UI;
using UnityEngine.UI;

public class testPlayer : MonoBehaviour {

    ClickButtonState[] blist;
	OneAxisState slider;
	TwoAxisState pad;
    bool isActive = false;

    public Text text;
	public Transform crt;

    public void ConnectDone(Controller con)
    {
        blist = new ClickButtonState[4];
		for (int i = 0; i < 4; i++) {
			blist [i] = (ClickButtonState) con.GetState (i + 1);
		}
		slider = (OneAxisState) con.GetState (5);
		pad = (TwoAxisState)con.GetState (6);
        isActive = true;
    }
    
	// Update is called once per frame
	void Update () {

        if (isActive)
        {
			string txt = "";
			Vector3 movement = Vector3.zero;

			Vector2 padInput = pad.GetVector2 ();
			if (padInput != Vector2.zero) {
				txt = string.Format ("x:{0}\ny:{1}", padInput.x, padInput.y);
				movement = new Vector3 (padInput.x, padInput.y);
			} else {
				if (blist [0].GetBool ()) {
					txt = txt + "left ";
					movement += new Vector3 (-1, 0);
				}
				if (blist [1].GetBool ()) {
					txt = txt + "up ";
					movement += new Vector3 (0, 1);
				}
				if (blist [2].GetBool ()) {
					txt = txt + "right ";
					movement += new Vector3 (1, 0);
				}
				if (blist [3].GetBool ()) {
					txt = txt + "down ";
					movement += new Vector3 (0, -1);
				}
				movement.Normalize ();
			}
			text.text = txt;
			crt.position += movement;

			crt.localScale = new Vector3 (1 - slider.GetFloat (), 1, 1);
        }
        else
        {
            text.text = "disable";
        }
	}
}
