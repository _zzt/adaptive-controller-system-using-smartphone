﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace ACS.App {
	public class SensorManager : MonoBehaviour {
		public bool gyroEnabled = false;

		ConnectionManager conn;

		void Start() {
			conn = ConnectionManager.Instance;

			conn.MessageReceived += ToggleSensor;
		}

		void ToggleSensor (byte[] data)
		{
			if (data.Length >= 100)
				return;

			string check = Encoding.Default.GetString(data);
			var splitdata = check.Split(':');
			if (splitdata.Length == 2) {
				int sensorType;
				bool value;
				if (!int.TryParse (splitdata [0], out sensorType) || !bool.TryParse(splitdata[1], out value)) {
					return;
				}
				switch (sensorType) {
				case -1:
					gyroEnabled = value;
					Input.gyro.enabled = value;
					break;
				case -2:
#if UNITY_ANDROID
                        Handheld.Vibrate ();
#endif
					break;
				}
			}
		}

		void Update() {
			if (gyroEnabled) {
				Quaternion gyroValue = Input.gyro.attitude;
				conn.Send (string.Format ("-1:{0},{1},{2},{3}", gyroValue.x, gyroValue.y, gyroValue.z, gyroValue.w));
			}
		}


	}
}
