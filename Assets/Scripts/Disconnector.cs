﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ACS.App {
	public class Disconnector : MonoBehaviour {

		public ToastMessage toast;
		public ConnectionManager conn;

		bool pressed = false;

		IEnumerator resetter;

		void Awake () {
			conn = FindObjectOfType<ConnectionManager> ();

			conn.Disconnected += NotifyAndReturn;
		}

		void NotifyAndReturn ()
		{
			SceneManager.LoadScene ("Main Scene");
		}

		void Update () {
			if (Input.GetKeyDown (KeyCode.Escape)) {
				if (!pressed) {
					pressed = true;
					resetter = ResetPressed ();
					StartCoroutine (resetter);

					#if !UNITY_EDITOR
					toast.showToastOnUiThread ("Press back button twice to disconnect");
					#else
					Debug.Log("Press back button twice to disconnect");
					#endif
				} else {
					StopCoroutine (resetter);
					conn.Disconnect ();
					SceneManager.LoadScene ("Main Scene");
				}
			}
		}

		IEnumerator ResetPressed() {
			yield return new WaitForSecondsRealtime (1f);
			pressed = false;
		}
	}
}
