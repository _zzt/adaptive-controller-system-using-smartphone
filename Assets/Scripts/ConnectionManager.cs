﻿using System.Collections;

using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

using ACS;
using UnityEngine.UI;
using System.Text;

namespace ACS.App
{
    public class ConnectionManager : MonoBehaviour
    {
        private static volatile ConnectionManager _instance;
        public static ConnectionManager Instance
        {
            get
            {
                Debug.Assert(_instance != null);
                return _instance;
            }
        }

        private void Awake()
        {
			if (_instance != null) {
				Destroy (this.gameObject);
				return;
			}
            _instance = this;

			DontDestroyOnLoad(this);
			Screen.sleepTimeout = SleepTimeout.SystemSetting;
        }

        public NetworkClient client;

        public delegate void ConnectionVoidEvent();
        public delegate void ConnectionMessageEvent(byte[] data);
        public event ConnectionVoidEvent Connected;
        public event ConnectionVoidEvent Disconnected;
        public event ConnectionMessageEvent MessageReceived;

        public bool isConnected = false;

        public void Initialize(ServerInformation info)
        {
            client = new NetworkClient();

            client.RegisterHandler(MsgType.Connect, OnConnected);
            client.RegisterHandler(MsgType.Disconnect, OnDisconnected);
            client.RegisterHandler(ConnectionConfiguration.ServerMessageType, OnMessageIncoming);
            client.Connect(info.LocalIP, ConnectionConfiguration.Port);
        }

        public void Disconnect()
		{
			isConnected = false;
			client.Disconnect ();
			client.Shutdown ();
        }

        public void Send(string str)
        {
            byte[] bt = Encoding.Default.GetBytes(str);
            Send(bt);
        }

        public void Send(byte[] data)
        {
            ByteBase bytebase = new ByteBase(data);
            client.Send(ConnectionConfiguration.ClientMessageType, bytebase);
        }

        void OnConnected(NetworkMessage msg)
        {
			isConnected = true;
			if (Connected != null)
				Connected ();
        }

        void OnDisconnected(NetworkMessage msg)
        {
			isConnected = false;
			if (Disconnected != null)
				Disconnected ();
        }

        void OnMessageIncoming(NetworkMessage msg)
        {
            ByteBase bytebase = new ByteBase(null);
            bytebase.Deserialize(msg.reader);
			byte[] data = bytebase.data;
			if (MessageReceived != null)
				MessageReceived (data);
        }
    }
}