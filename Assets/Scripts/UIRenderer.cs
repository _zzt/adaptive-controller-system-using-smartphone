﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using UnityEngine;
using ACS.UI;
using UnityEngine.UI;

namespace ACS.App {
	public class UIRenderer : MonoBehaviour {

		public ConnectionManager connManager;

		public RectTransform UIRoot;

		bool receiving = false;
		byte[] uiData;
		int dataSize;
		int index;

		void Awake () {
			foreach (RectTransform child in UIRoot) {
				Destroy (child.gameObject);
			}

			connManager = ConnectionManager.Instance;

			connManager.MessageReceived += InitializeControlUI;
            connManager.MessageReceived += SetActiveUI;

        }

		void OnDestroy() {
			connManager.MessageReceived -= InitializeControlUI;
            connManager.MessageReceived -= SetActiveUI;

        }

		void InitializeControlUI(byte[] data) {
			if (!receiving) {
				string check = Encoding.Default.GetString (data);
				var splitdata = check.Split (':');
				if (splitdata.Length != 2)
					return;

				int tag;
				if (!int.TryParse (splitdata [0], out tag))
					return;
				if (tag != 0)
					return;

				int length;
				if (!int.TryParse (splitdata [1], out length))
					return;

				receiving = true;
				dataSize = length;
				index = 0;
				uiData = new byte[dataSize];
			} else {
				if (data.Length < 100)
					return;

				Array.Copy (data, 0, uiData, index, data.Length);
				index += data.Length;
				if (index == dataSize) {
					receiving = false;

					RenderUI (uiData);
				}
			}
		}

        void SetActiveUI(byte[] data)
        {
            if (data.Length >= 100)
                return;

            string check = Encoding.Default.GetString(data);
            var splitdata = check.Split(':');
            if (splitdata.Length == 2)
            {
                int sensorType;
                bool value;
                if (!int.TryParse(splitdata[0], out sensorType) || !bool.TryParse(splitdata[1], out value))
                {
                    return;
                }
                if(sensorType > 0)
                {
                    var gkuyl =UIRoot.GetComponentsInChildren<UITemplateBase>();

                    foreach(var tfgyk in gkuyl)
                    {
                        if(tfgyk.uid == sensorType)
                        {
                            if(!(tfgyk is ImageScript))
                            {
                                break;
                            }
                            tfgyk.GetComponent<RawImage>().enabled = value;
                            break;
                        }
                    }
                }
            }
        }

        void RenderUI(byte[] data) {
			var list = UIXMLParser.Instance.Decode (data);

			if (list != null) {
				HashSet<int> remains = UISerializer.UpdateInsts (list, UIRoot);
				foreach (RectTransform child in UIRoot) {
					UITemplateBase ui = child.GetComponent<UITemplateBase> ();
					if (ui != null && !remains.Contains (ui.uid)) {
						Destroy (child.gameObject);
					}
				}
			}
		}
	}
}