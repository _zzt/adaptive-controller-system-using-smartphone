﻿using UnityEngine;
using System.Collections;
using System.Text;

namespace ACS.App {
	public class Sender : MonoBehaviour
	{
		private static volatile Sender _instance;
		public static Sender Instance
		{
			get
			{
				return _instance;
			}
		}

		void Awake() {
			if (_instance != null) {
				Destroy (this);
			}
			_instance = this;
		}

		public void SendSignal(string message) {
			ConnectionManager.Instance.Send (message);
		}
	}
}
