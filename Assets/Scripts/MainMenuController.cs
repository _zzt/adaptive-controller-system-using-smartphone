﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuController: MonoBehaviour {

    public GameObject main;
    public GameObject search;
    

	// Use this for initialization
	void Start () {
        main.SetActive(true);
        search.SetActive(false);

        //Screen.orientation = ScreenOrientation.LandscapeLeft;
	}
	
    public void GetOut()
    {
        Application.Quit();
    }

    public void GoNext()
    {
        main.SetActive(false);
        search.SetActive(true);
    }

    public void GoBack()
    {
        main.SetActive(true);
        search.SetActive(false);
    }

    int count = 0;

    public void ChangeUI()
    {
        GameObject rsc = Resources.Load<GameObject>("testprefab2");

        var obj = GameObject.Instantiate(rsc) as GameObject;
        var rect = obj.GetComponent<RectTransform>();
        rect.SetParent(main.transform);       
        
        obj.GetComponent<RectTransform>().position = new Vector3(445,count);

        count += 50;
    }
}
