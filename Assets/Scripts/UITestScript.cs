﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Text;

namespace ACS.UI
{
    public class UITestScript : MonoBehaviour
    {
        public Canvas main;
        public GameObject objpr;
        public GameObject TEMP;
        public UITemplateBase ThatObj;

        public void SaveUI()
        {
            UISerializedDataList list = new UISerializedDataList();

            var l = objpr.GetComponentsInChildren<UITemplateBase>();

            foreach (var child in l)
            {
                UISerializedData a = new UISerializedData(child, main.pixelRect);
                a.uid = child.uid;

                list.list.Add(a);
            }
            
            UISerializer.SaveUIDataToXml(list);
        }

        public void LodeUI()
        {
            UISerializedDataList a = UISerializer.LoadUIDataFromFile("asdf");

            UISerializer.Insts(a, TEMP.transform);
        }
    }
}