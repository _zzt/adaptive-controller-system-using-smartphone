﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using ACS;
using UnityEngine.SceneManagement;

namespace ACS.App
{
    public class CameraScript : MonoBehaviour
    {

        static WebCamTexture backCam;

        public QRCodeReader reader;

		public Text text;
        public Button goBackButton;

		ConnectionManager connManager;

        void OnEnable()
        {
            if (backCam == null)
            {
                backCam = new WebCamTexture();
            }

            //GetComponent<Renderer>().material.mainTexture = backCam;
            GetComponent<RawImage>().material.mainTexture = backCam;

			connManager = ConnectionManager.Instance;
            connManager.Connected += OnConnect;

            reader.QRCodeFound += QRCodeFound;
            if (!backCam.isPlaying)
                StartScan();
        }

        void OnDisable()
        {
			connManager.Connected -= OnConnect;
			reader.QRCodeFound -= QRCodeFound;
            reader.StopScan();
            backCam.Stop();
        }

        public void StartScan()
        {
            backCam.Play();
            reader.StartScan(backCam);
        }

        public void StopScan()
        {
            reader.StopScan();
            backCam.Stop();
        }

        void QRCodeFound(string data)
        {
#if !UNITY_STANDALONE_WIN
            Handheld.Vibrate();
#endif

            StopScan();

			text.text = "Connecting...";
            goBackButton.enabled = false;
			connManager.Disconnected += CannotConnect;
            connManager.Initialize(Decode(data));
        }

        void CannotConnect ()
		{
			connManager.Disconnected -= CannotConnect;
			text.text = "Cannot connect to game\nTry another QR code";
			StartScan ();
			goBackButton.enabled = true;
        }

        ServerInformation Decode(string data)
        {
            QRCodeDataProcessor processor = QRCodeDataProcessor.Instance;
            return processor.Deserialize(processor.Decompress(data));
        }

        void OnConnect()
		{
			connManager.Disconnected -= CannotConnect;
            SceneManager.LoadScene("ControllerTestScene");
        }
    }

}