﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Text;
using ACS.UI;

namespace ACS.Library {
	public class test : MonoBehaviour {
		public Text textfield;
		public RawImage QRImg;
		public ConnectionManager connManager;
		public ControllerManager controllers;
		public Listener listener;
		public Sender sender;
		public InputField inputfield;
		public Dropdown connIdSelector;
		public TextAsset UIXML;

        public testPlayer testplayer;

		public void OpenServer() {
			textfield.text += NetworkInspector.Instance.GetLocalIP ();

			QRImg.texture = QRCodeGenerator.Instance.Generate ();

			connManager.Run ();
			connManager.Open ();

			listener.Connected += RefreshConnIdOptions;
			listener.Connected += (Controller con) => textfield.text += "\nConnected(" + con.connectionId + ")";
			listener.Disconnected += RefreshConnIdOptions;
			listener.Disconnected += (Controller con) => textfield.text += "\nDisconnected(" + con.connectionId + ")";
			listener.MessageReceived += OnMessageReceived;
		}

		void OnMessageReceived (Controller con, byte[] data)
		{
			//textfield.text += "\n(" + con.connectionId + ") " + Encoding.UTF8.GetString (data);
		}

		void RefreshConnIdOptions (Controller _)
		{
			connIdSelector.ClearOptions ();
			List<Dropdown.OptionData> newOptions = new List<Dropdown.OptionData> ();
			for (int i = 0; i < controllers.maxControllers; i++) {
				Controller con = controllers.Get(i);
				if (con != null) {
					newOptions.Add(new Dropdown.OptionData(con.connectionId.ToString()));
				}
			}
			connIdSelector.AddOptions (newOptions);
		}

		public void PortTestClick() {
			Debug.Log (NetworkServer.listenPort);
		}

		public void SendButtonClick() {
			string str = inputfield.text;
			byte[] data = Encoding.UTF8.GetBytes(str);
			Controller con = GetSelectedController (connIdSelector);
			sender.Send (con, data);
		}

		public void SendUIButtonClick() {
			Controller con = GetSelectedController (connIdSelector);
			sender.Send (con, UIXML);

            //testplayer.ConnectDone(con);
        }

		Controller GetSelectedController(Dropdown selector) {
			int connId = int.Parse (selector.options [selector.value].text);
			Controller con = controllers.LookupConnectionId (connId);
			return con;
		}
	}
}