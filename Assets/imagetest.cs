﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using ACS.Library;

public class imagetest : MonoBehaviour
{

    public RawImage qrImg;

    public TextAsset controller;

    public Text sss;

    // Use this for initialization
    void Start()
    {
        ConnectionManager conn = ConnectionManager.Instance;
        conn.Run();
        conn.Open();

        qrImg.texture = QRCodeGenerator.Instance.Generate();

        Listener listener = Listener.Instance;

        listener.Connected += OnConnect;
    }

    bool fflag = false;
    bool fffff = true;
    int count = 0;

    Controller ccc;

    void OnConnect(Controller con)
    {
        Sender.Instance.Send(con, controller);
        fflag = true;
        count = 24;
        ccc = con;
    }

    // Update is called once per frame
    void Update()
    {
        if (fflag)
        {
            count--;

            if(count < 0)
            {
                count = 24;
                ccc.SetUIActive(11, fffff);

                sss.text = fffff.ToString();

                fffff = !fffff;
            }

        }

        
    }

}
